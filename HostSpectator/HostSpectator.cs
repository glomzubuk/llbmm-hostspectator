﻿using System;
using UnityEngine;
using Multiplayer;

namespace HostSpectator
{
    public class HostSpectator : MonoBehaviour
    {
        #region modinfos
        public const string modVersion = "v0.1";
        #endregion
        public static HostSpectator instance = null;
        private static ModMenuIntegration MMI = null;

        public static void Initialize()
        {
            GameObject gameObject = new GameObject("HostSpectator");
            instance = gameObject.AddComponent<HostSpectator>();
            DontDestroyOnLoad(gameObject);
        }

        private static KeyCode ToggleSpectatorKey
        {
            get
            {
                if (MMI != null)
                    try
                    {
                        return MMI.GetKeyCode("(key)toggleSpectatorKey");
                    } catch { }
                return KeyCode.P;
            }
        }
        public static JOFJHDJHJGI CurrentGameState
        {
            get
            {
                return DNPFJHMAIBP.HHMOGKIMBNM();
            }
        }
        public static GameMode CurrentGameMode
        {
            get
            {
                return JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN;
            }
        }

        public static int LocalPlayerNumber
        {
            get
            {
                return P2P.localPeer.playerNr;
            }
        }


        void Update()
        {
            if (MMI == null) { MMI = gameObject.AddComponent<ModMenuIntegration>(); }

            if (CurrentGameState == JOFJHDJHJGI.NBNDONMABLK && CurrentGameMode != GameMode._1v1 && Input.GetKeyDown(ToggleSpectatorKey))
            {
                DNPFJHMAIBP.GKBNNFEAJGO(new Message(Msg.SEL_SPECTATOR, 0, LocalPlayerNumber, null, -1));
            }
        }
    }
}
